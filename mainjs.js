const cod = document.getElementById("codigo")
const marca = document.getElementById("marca")
const modelo = document.getElementById("modelo")
const anio = document.getElementById("year")
const fecha_inicial = document.getElementById("fecha_inicial")
const fecha_final = document.getElementById("fecha_final")
const form = document.getElementById("form")
const parrafo = document.getElementById("warnings")




form.addEventListener("submit", e=>{
    e.preventDefault()
    let warnings = ""
    let condicion = false
    let vcodigo= /^[a-zA-Z0-9]{1,5}$/
    let vmarca=/^[a-zA-Z0-9]{1,50}$/
    let vmodelo=/^[a-zA-Z0-9]{1,30}$/
    let vyear=/^[0-9]{1,4}$/
    let vfecha=/^[0-9]{1,4}$/
    parrafo.innerHTML = ""



    if(cod.value.length >5 || !vcodigo.test(cod.value)){
        warnings += `El Codigo no es valido.<br>`
        condicion = true
    }

    if(marca.value.length >50 || !vmarca.test(marca.value)){
        warnings += `La Marca no es valido.<br>`
        condicion = true
    }

    if(modelo.value.length >30 || !vmodelo.test(modelo.value)){
        warnings += `El Modelo no es valido.<br>`
        condicion = true
    }
    
    if(anio.value.length >4 || !vyear.test(anio.value)){
        warnings += `El año no es valido .<br>`
        condicion = true
    }

    if(fecha_inicial.value>fecha_final.value || !vfecha.test(fecha_final.value)){
        warnings += `fecha final incorrecta.<br>`
        condicion = true
    }

    if(condicion){
        parrafo.innerHTML = warnings
    }else{
        parrafo.innerHTML = "Enviado con exito"
    }

})